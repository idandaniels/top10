# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 13:51:56 2020

@author: idan
"""

from google2pandas import *
conn = GoogleAnalyticsQuery(secrets='client_secrets.json', token_file_name='analytics.dat')
import pandas as pd
import time

start = time.time()
startIndex = 1
maxResults = 10000
df_list = []

query = {\
            'ids': '182688681',
            'metrics':['totalEvents'],
            'dimensions':['date', 'dimension1', 'dimension2', 'country', 'pagePath', 'deviceCategory', 'eventLabel'],
            'start_date': '2019-12-25',
            'end_date': '2020-02-25',
            'start_index': startIndex,
            'max_results': maxResults,
            'filters': 'ga:eventCategory==redirect'
        }

# getting the max results variable
df_first, metadata = conn.execute_query(**query)
total_results = metadata.get('totalResults')
print('The total rows are %d' %total_results)

# Appending the first df to the list of data frames
df_list.append(df_first)
print('start index is %d, and max result is %d' % (startIndex, maxResults))
print('df_list has %d items' % len(df_list))

# End of first iteration
# preparing for the 2nd iteration and so on
# check if total results is larger then 10K, or stop after the first iteration
try:
    while total_results > maxResults:
        startIndex += 10000
        temp_subs = total_results - maxResults
        if temp_subs >= 10000:
            maxResults += 10000
        else:
            maxResults = total_results
        query = {\
                'ids': '182688681',
                'metrics':['totalEvents'],
                'dimensions':['date', 'dimension1', 'dimension2', 'country', 'pagePath', 'deviceCategory', 'eventLabel'],
                'start_date': '2020-12-25',
                'end_date': '2020-02-25',
                'start_index': startIndex,
                'max_results': maxResults,
                'filters': 'ga:eventCategory==redirect'
            }
        df_first, metadata = conn.execute_query(**query)
        df_list.append(df_first)
        print('start index is %d, and max result is %d' % (startIndex, maxResults))
        print('df_list has %d items' % len(df_list))
except Exception as e:
    print(str(e))
    

new_df = pd.concat(df_list) 
print(new_df.info())
print(new_df.head(20))

   
    
